//Clase que contiene el Modelo de la aplicación. 
function Model() {
	this.tbPublicacion=null;
	this.emailUsuario = null;
}

Model.prototype = {
	
	
	//Obtención de las publicaciones de los amigos:
	loadamigos : function() {
		this.tbPublicacion = PublicacionServicesRs.getPublicacionesAmigos({
			emailU : this.emailUsuario
			
		});
				
	}
};

//Clase que contiene la gestión de la capa Vista
function View(model) {
	// referencia al modelo
	this.model = model;
}

View.prototype = {
		
		//Para crear y rellenar la tabla que contendrá las publicaciones de los amigos:
		listamigos : function() {
			
			$("#tblList").html("");
			$("#tblList").html("<thead>" + "<tr>" + "<th></th>"
				+ "<th>Texto</th>" + "<th>Título</th>" + "<th>Fecha</th>"
				+ "</tr>" + "</thead>" + "<tbody>" + "</tbody>");
			for ( var i in this.model.tbPublicacion) {
				var publi = this.model.tbPublicacion[i];
				$("#tblList tbody").append(
					"<tr><td></td>" 
					+ "<td>" + publi.titulo
					+ "</td>" + "<td>" + publi.texto + "</td>" + 
					"<td>" + publi.fecha +"</td>");
			}
		}

	
};

$(function() {
	//Creamos el modelo con los datos y la conexión al servicio web:
	var model = new Model();
	//Obtenemos el email del usuario correspondiente para poder usarlo como parámetro:
	model.emailUsuario = sessionStorage.getItem("user");
	//Creamos la vista que incluye acceso al modelo.
	var view = new View(model);
	
	//Cargamos el listado de publicaciones de amigos:
	model.loadamigos();
	//Y creamos la tabla que se mostrará:
	view.listamigos();
	
	//Actualizara la vista de publicaciones de amigos cada 30 segundos de modo 
	//que registre una nueva publicacion de un amigo en caso de haberla
	setInterval(function(){
	    model.loadamigos();
	    view.listamigos();
	}, 30000);
	
	
	

	
});
