//Clase que contiene el Modelo de la aplicación. 
function Model() {
	this.emailUsuario = null;
	this.tbInvis = null;
}

Model.prototype = {
		
		loadInvis : function() {
			//Obtenemos la lista de invitaciones
			this.tbInvis = AmigoServicesRs.InvisRecibidas({
				emailU : this.emailUsuario
			});
		},
		
		acceptInvi : function (emailAmigo) {
			//Llamada al servidor
			AmigoServicesRs.aceptaInvi({
				emailU : this.emailUsuario,
				emailA : emailAmigo
			});
		}


};

//Clase que contiene la gestión de la capa Vista
function View(model) {
	// referencia al modelo
	this.model = model;
}

View.prototype = {
		
		listInvis : function() {
			$("#tblListInvis").html("");
			$("#tblListInvis").html("<thead>" + "<tr>"
				+ "<th>Email</th>" + "<th>Nombre</th>" + "<th>Aceptar invitación</th>"
				+ "</tr>" + "</thead>" + "<tbody>" + "</tbody>");
			for ( var i in this.model.tbInvis) {
				var usuario = this.model.tbInvis[i];
				$("#tblListInvis tbody").append(
					"<tr><td>" + usuario.email
					+ "</td>" + "<td>" + usuario.nombre + "</td>" + 
					"<td><button type='submit' class='myButton'>Aceptar</button></tr>");
			}
		},
		
		acceptInvi : function(celda) {
			
			//Obtenemos el correo del amigo que queremos hacer
			correoAmigo = celda.closest('tr').find('td').get(0).innerHTML;
			
			//Llamada a la función del modelo
			this.model.acceptInvi(correoAmigo);
			
		}
};

$(function() {
	//Creamos el modelo con los datos y la conexión al servicio web.
	var model = new Model();
	model.emailUsuario = sessionStorage.getItem("user");
	//Creamos la vista que incluye acceso al modelo.
	var view = new View(model);

	model.loadInvis();
	view.listInvis();
	
	//Manejador para el boton para mandar petición de amistad
	$(".myButton").click(function() {
		view.acceptInvi($(this));
		window.location.href = "listadoInvis.html";
	});
	
	//Actualizara la vista de peticiones de amigos cada 30 segundos de modo 
	//que registre una nueva peticion de un usuario en caso de haberla
	setInterval(function(){
	    model.loadInvis();
	    view.listInvis();
	}, 30000);
	
});