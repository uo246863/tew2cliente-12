//Clase que contiene el Modelo de la aplicación. 
function Model() {
	this.tbUsuarios=null;
	this.deleted=null;
}

Model.prototype = {
	//Reinicia la base de datos
	loadBD: function(){
		UsuariosServicesRs.reiniciarBD();
	},
	//Obtenemos los usuarios
	load : function() {
		this.tbUsuarios = UsuariosServicesRs.getUsuarios();
	},
	
	// Eliminación un usuario existente
	remove : function(correo) {
		// Llamamos al servicio de borrado de usuario
		this.deleted = UsuariosServicesRs.deleteUsuario({
			email : correo
		});
		// Recargamos la lista de usuarios.
		this.load();
	}
	
};

//Clase que contiene la gestión de la capa Vista
function View(model) {
	// referencia al modelo
	this.model = model;
}

View.prototype = {
		
		list : function() {

			$("#tblList").html("");
			$("#tblList").html("<thead>" + "<tr>" + "<th></th>"
				+ "<th>Email</th>" + "<th>Nombre</th>" + "<th>Seleccionar varios</th>"
				+ "</tr>" + "</thead>" + "<tbody>" + "</tbody>");
			for ( var i in this.model.tbUsuarios) {
				var usuario = this.model.tbUsuarios[i];
				$("#tblList tbody").append(
					"<tr><td><img src='icons/delete.png' class='btnDelete'/></td>" 
					+ "<td>" + usuario.email
					+ "</td>" + "<td>" + usuario.nombre + "</td>" + 
					"<td><input type='checkbox' class='myCheckbox' value='select'></td></tr>");
			}
		},
		
		remove : function(celda) {
			
			// Accedemos a la fila que está por encima de esta celda (closest('tr'))
			// y despues obtenemos todas las celdas de esa fila (find('tr')) y
			// nos quedamos con la segunda (get(1)) que es la contiene el "email" del
			// usuario. Al ser un string no requerimos parsearlo
			correo = celda.closest('tr').find('td').get(1).innerHTML;
			if (correo == "admin@email.com") {
				alert("No se puede borrar el administrador");
				return;
			}
			//Lo pasamos al modelo para que lo borre
			this.model.remove(correo);
		} 
};

$(function() {
	//Creamos el modelo con los datos y la conexión al servicio web.
	var model = new Model();
	//Creamos la vista que incluye acceso al modelo.
	var view = new View(model);
	
	model.load();
	view.list();
	
	//Manejador para el evento de delete
	$("#tblList").on("click", ".btnDelete", function(event) {
		//Ordena borrar el usuario y cargar la nueva lista
		view.remove($(this));
		if (!model.deleted) {
			alert("No se puede borrar un usuario en sesion");
		}
		view.list();
	});
	
	
	$("#borraSeleccion").on("click", function(event) {
		//Obtenemos todos los checkbox seleccionados y para cada uno llamamos al remove sobre la celda
		$('.myCheckbox:checkbox:checked').each(function () {
			view.remove($(this));
			if (!model.deleted) {
				alert("No se puede borrar un usuario en sesion");
			}
		});
		view.list();
		
	});
	
});
