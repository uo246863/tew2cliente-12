//Clase que contiene el Modelo de la aplicación. 
function Model() {
	this.tbNoAmigos = null;
	this.emailUsuario = null;
}

Model.prototype = {
		//Obtenemos los usuarios que no son amigos
		loadNoAmigos : function() {
			//Obtenemos la tabla de amigos
			this.tbNoAmigos = AmigoServicesRs.NoAmigosUsuario({
				emailUser : this.emailUsuario
			});
			
		},
		
		sendInvite : function (emailAmigo) {
			//Llamada al servidor
			AmigoServicesRs.creaInvi({
				emailU : this.emailUsuario,
				emailA : emailAmigo
			});
		}
};

//Clase que contiene la gestión de la capa Vista
function View(model) {
	// referencia al modelo
	this.model = model;
}

View.prototype = {
		
		listNoAmigos : function() {

			$("#tblList").html("");
			$("#tblList").html("<thead>" + "<tr>"
				+ "<th>Email</th>" + "<th>Nombre</th>" + "<th>Mandar invitación</th>"
				+ "</tr>" + "</thead>" + "<tbody>" + "</tbody>");
			for ( var i in this.model.tbNoAmigos) {
				var usuario = this.model.tbNoAmigos[i];
				$("#tblList tbody").append(
					"<tr><td>" + usuario.email
					+ "</td>" + "<td>" + usuario.nombre + "</td>" + 
					"<td><button type='submit' class='myButton'>Petición de amistad</button></tr>");
			}
		},
		
		sendInvite : function(celda) {
			
			//Obtenemos el correo del amigo que queremos hacer
			correoAmigo = celda.closest('tr').find('td').get(0).innerHTML;
			
			if (correoAmigo == this.model.emailUsuario) {
				alert("No te puedes mandar una petición de amistad a ti mismo");
				return;
			}
			
			//Llamada a la función del modelo
			this.model.sendInvite(correoAmigo);
			
		}
};

$(function() {
	//Creamos el modelo con los datos y la conexión al servicio web.
	var model = new Model();
	model.emailUsuario = sessionStorage.getItem("user");
	//Creamos la vista que incluye acceso al modelo.
	var view = new View(model);
	
	model.loadNoAmigos();
	view.listNoAmigos();

	
	//Manejador para el evento de la search bar y filtrado de la tabla
	$("#search").on("keyup", function() {
		var value = $(this).val();
		$("#tblList tr").filter(function() {
			$(this).toggle($(this).text().indexOf(value) > -1);
		});
	});
	
	//Manejador para el boton para mandar petición de amistad
	$(".myButton").click(function() {
		view.sendInvite($(this));
		window.location.href = "listadoAmigos.html";
	});
	
	//Función que actualiza la vista cada 30s por si algun admin borre un usuario
	setInterval(function(){
	    model.loadNoAmigos();
	    view.listNoAmigos();
	}, 30000);
	
	
});