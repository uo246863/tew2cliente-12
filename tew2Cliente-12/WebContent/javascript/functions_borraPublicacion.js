//Clase que contiene el Modelo de la aplicación. 
function Model() {
	this.tbPublicacion=null;
	this.emailUsuario = null;
	
	
}

Model.prototype = {
	
	
	//Obtenemos las publicaciones del usuario:
	load : function() {
		this.tbPublicacion = PublicacionServicesRs.getMisPublicaciones({
			emailU : this.emailUsuario
		});
				
	},
	borraPublicacion : function(title, text, date) {
		PublicacionServicesRs.borrarPublicacion({
			texto : text,
			titulo : title,
			fecha : date,
			email : this.emailUsuario
		});
	}
};

//Clase que contiene la gestión de la capa Vista
function View(model) {
	// referencia al modelo
	this.model = model;
}

View.prototype = {
		
		//Creamos y rellenamos la tabla que contendrá las publicaciones del usuario:
		list : function() {

			$("#tblList").html("");
			$("#tblList").html("<thead>" + "<tr>" + "<th></th>"
				+ "<th>Texto</th>" + "<th>Título</th>" + "<th>Fecha</th>" 
				+ "<th>Seleccionar varios a borrar</th>"
				+ "</tr>" + "</thead>" + "<tbody>" + "</tbody>");
			for ( var i in this.model.tbPublicacion) {
				var publi = this.model.tbPublicacion[i];
				$("#tblList tbody").append(
					"<tr><td></td>" 
					+ "<td>" + publi.titulo
					+ "</td>" + "<td>" + publi.texto + "</td>" + 
					"<td>" + publi.fecha +"</td>" +
					"<td><input type='checkbox' class='myCheckbox' value='select'></td></tr>");
			}
		},
		
		borraPublicacion : function(celda) {
			
			var text =  celda.closest('tr').find('td').get(1).innerHTML;
			console.log(text);
			var title = celda.closest('tr').find('td').get(2).innerHTML;
			console.log(title);
			var date = celda.closest('tr').find('td').get(3).innerHTML;
			console.log(date);
			
			this.model.borraPublicacion(text, title, date);
		}

	
};

$(function() {
	//Creamos el modelo con los datos y la conexión al servicio web.
	var model = new Model();
	//Obtenemos el email del usuario correspondiente para poder usarlo como parámetro:
	model.emailUsuario = sessionStorage.getItem("user");
	//Creamos la vista que incluye acceso al modelo.
	var view = new View(model);
	
	//Cargamos las publicaciones del usuario:
	model.load();
	//Y llamamos a la función correspondiente para listarlas:
	view.list();
	
	//Manejador para el evento de borrado
	$("#borraSeleccion").on("click", function(event) {	
		//Obtenemos todos los checkbox seleccionados y para cada uno llamamos al remove sobre la celda
		$('.myCheckbox:checkbox:checked').each(function () {
			view.borraPublicacion($(this));
		});
		model.load();
		view.list();
	});
	
});