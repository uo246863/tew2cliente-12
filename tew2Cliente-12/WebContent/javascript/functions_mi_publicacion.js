//Clase que contiene el Modelo de la aplicación. 
function Model() {
	this.tbPublicacion=null;
	this.emailUsuario = null;
}

Model.prototype = {
	
	
	//Obtenemos las publicaciones del usuario:
	load : function() {
		this.tbPublicacion = PublicacionServicesRs.getMisPublicaciones({
			emailU : this.emailUsuario
		});
				
	},
	
	//Creamos una publicación nueva para el usuario:
	crearPublicacion : function(titulo, texto, email) {
		PublicacionServicesRs.crearPublicacion({
			titulo: titulo,
			texto: texto,
			email: this.emailUsuario
		});
	}
};

//Clase que contiene la gestión de la capa Vista
function View(model) {
	// referencia al modelo
	this.model = model;
}

View.prototype = {
		
		//Creamos y rellenamos la tabla que contendrá las publicaciones del usuario:
		list : function() {

			$("#tblList").html("");
			$("#tblList").html("<thead>" + "<tr>" + "<th></th>"
				+ "<th>Texto</th>" + "<th>Título</th>" + "<th>Fecha</th>"
				+ "</tr>" + "</thead>" + "<tbody>" + "</tbody>");
			for ( var i in this.model.tbPublicacion) {
				var publi = this.model.tbPublicacion[i];
				$("#tblList tbody").append(
					"<tr><td></td>" 
					+ "<td>" + publi.titulo
					+ "</td>" + "<td>" + publi.texto + "</td>" + 
					"<td>" + publi.fecha +"</td>");
			}
		},
		
		//Guardamos las variables introducidas en el formulario html y llamamos a la función correspondiente:
		crearPublicacion: function() {
			tit = $("#titulo").val();
			tex = $("#texto").val();
			
			this.model.crearPublicacion(tit,tex,this.emailUsuario);
		}

	
};

$(function() {
	//Creamos el modelo con los datos y la conexión al servicio web.
	var model = new Model();
	//Obtenemos el email del usuario correspondiente para poder usarlo como parámetro:
	model.emailUsuario = sessionStorage.getItem("user");
	//Creamos la vista que incluye acceso al modelo.
	var view = new View(model);
	
	//Cargamos las publicaciones del usuario:
	model.load();
	//Y llamamos a la función correspondiente para listarlas:
	view.list();
	
	//Función que se encargará de crear la publicación en la base de datos cuando se haga click en el botón de submit: 
	$("#submit").on("click", function(event) {   
		view.crearPublicacion();
		
	});

	
});
