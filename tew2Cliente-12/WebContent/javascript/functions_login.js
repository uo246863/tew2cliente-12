//Clase que contiene el Modelo de la aplicación. 
function Model() {
	this.usuario = null;
}

Model.prototype = {
	// LLama a la funcion verify de loginservicesRS y carga el usuario que
	// devuelve
	login : function(email, password) {
		this.usuario = LoginServicesRs.verify({
			login: email,
			passwd: password
		});
	},
	//Realiza el logout
	logout : function(emailUsuario) {
		LoginServicesRs.logout({
			emailU : emailUsuario
		});
	},
	
	//Reinicia la base de datos
	loadBD: function(){
		UsuariosServicesRs.reiniciarBD();
	},
};

//Clase que contiene la gestión de la capa Vista
function View(model) {
	// referencia al modelo
	this.model = model;
}

View.prototype = {
		
		login : function() {

			// Cogemos los datos del login
			login = $("#username").val();
			passwd = $("#passwd").val();
			
			
			// Llamamos al metodo del modelo
			this.model.login(login, passwd);	
		}
};

$(function() {
	//Creamos el modelo con los datos y la conexión al servicio web.
	var model = new Model();
	//Creamos la vista que incluye acceso al modelo.
	var view = new View(model);
	
	//Manejador para el evento de submit
	$("#login").bind("submit", function(event) {
		//Llamamos a la función de login del servidor
		view.login();
		if (model.usuario != "") {
			sessionStorage.setItem("user", model.usuario.email);
			if (model.usuario.rol == "admin") {
				window.location.href = "opciones-admin.html";
			}
			if (model.usuario.rol == "usuario") {
				window.location.href = "opciones-usuario.html";
			}
		}
		
		else {
			alert("Email o contraseña incorrectos");
		}
	});
	
	$("#logout").on("click", function(event) {
		
		//Obtenemos el email del usuario
		var emailUsuario = sessionStorage.getItem("user");
		//Llamamos a la función del modelo y recargamos la vista
		model.logout(emailUsuario);
		window.location.href = "index.html";
	});
	
	$("#reinicioBD").on("click", function(event) {
		//Llamamos a la función de reinicio de BD:
		alert("Reiniciando base de datos.");   
		model.loadBD();
		//Recargamos la página para que se muestre la nueva lista
		window.location.href = "opciones-admin.html";
		
	});
	
});